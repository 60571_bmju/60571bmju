-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Фев 27 2021 г., 22:34
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60571bmju`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Выдача`
--

CREATE TABLE `Выдача` (
  `ID` int NOT NULL COMMENT 'ID выдачи',
  `ID_экземпляра` int NOT NULL,
  `ID_читателя` int NOT NULL,
  `Дата_выдачи` date NOT NULL,
  `Дата_возврата_план` date NOT NULL COMMENT 'Плановая',
  `Дата_возврата_факт` date DEFAULT NULL COMMENT 'Фактическая'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Выдача`
--

INSERT INTO `Выдача` (`ID`, `ID_экземпляра`, `ID_читателя`, `Дата_выдачи`, `Дата_возврата_план`, `Дата_возврата_факт`) VALUES
(31, 1, 2, '2021-01-19', '2021-02-01', NULL),
(32, 4, 1, '2020-12-17', '2021-01-13', NULL),
(33, 8, 3, '2021-01-05', '2021-01-30', '2021-02-02'),
(34, 13, 4, '2021-02-01', '2021-02-17', NULL),
(35, 11, 5, '2020-12-15', '2020-12-24', '2021-02-08'),
(36, 10, 6, '2020-11-17', '2020-12-01', '2021-01-06'),
(37, 19, 13, '2021-02-01', '2021-02-14', NULL),
(38, 12, 10, '2021-02-07', '2021-02-21', '2021-02-19'),
(39, 7, 9, '2021-01-12', '2021-01-26', '2021-02-19'),
(40, 16, 2, '2021-01-06', '2021-01-20', '2021-02-18');

-- --------------------------------------------------------

--
-- Структура таблицы `Издание`
--

CREATE TABLE `Издание` (
  `ID` int NOT NULL COMMENT 'ID издания',
  `Наименование` varchar(255) DEFAULT NULL,
  `Автор` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Издание`
--

INSERT INTO `Издание` (`ID`, `Наименование`, `Автор`) VALUES
(1, 'Элементарная геометрия', 'Ж. Адамар'),
(2, 'Сборник заданий по высшей математике', 'Л.А. Кузнецов'),
(3, 'Геометрические преобразования', 'И.М. Яглом'),
(4, 'Микросварка давлением', 'Ю.Л. Красулин'),
(5, 'Объектно-ориентированное программирование', 'Э. Гамма'),
(6, 'Чистый код. Создание, анализ и рефакторинг', 'Р.К. Мартин'),
(7, 'JavaScript: сильные стороны', 'Д. Крокфорд'),
(8, 'Шаблоны корпоративных приложений', 'М. Фаулер'),
(9, 'Совершенный код. Мастер-класс', 'С. Макконнелл'),
(10, 'Паттерны проектирования', 'Э. Фримен');

-- --------------------------------------------------------

--
-- Структура таблицы `Читатель`
--

CREATE TABLE `Читатель` (
  `ID` int NOT NULL COMMENT 'ID читателя',
  `ФИО` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Читатель`
--

INSERT INTO `Читатель` (`ID`, `ФИО`) VALUES
(1, 'Иванов И.И'),
(2, 'Смирнов Г.Л.'),
(3, 'Петров Л.В.'),
(4, 'Созонова Д.П.'),
(5, 'Алексеев К.В.'),
(6, 'Петрухин Т.В.'),
(7, 'Щепкина У.П.'),
(8, 'Мартынова С.А.'),
(9, 'Орлов З.Ф.'),
(10, 'Рыбакин Р.И.'),
(11, 'Воробьев У.П.'),
(12, 'Некрасова С.С.'),
(13, 'Шишкин З.Т.'),
(14, 'Васильев К.Р.'),
(15, 'Федоров Е.В.');

-- --------------------------------------------------------

--
-- Структура таблицы `Экземпляр`
--

CREATE TABLE `Экземпляр` (
  `ID` int NOT NULL COMMENT 'ID экземпляра',
  `ID_издания` int NOT NULL,
  `Коэффициент_износа` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Экземпляр`
--

INSERT INTO `Экземпляр` (`ID`, `ID_издания`, `Коэффициент_износа`) VALUES
(1, 1, 3),
(2, 1, 1),
(3, 1, 2),
(4, 2, 4),
(5, 2, 3),
(6, 2, 5),
(7, 4, 2),
(8, 5, 2),
(9, 5, 1),
(10, 6, 4),
(11, 7, 3),
(12, 7, 2),
(13, 7, 5),
(14, 8, 2),
(15, 8, 1),
(16, 9, 3),
(17, 8, 2),
(18, 9, 4),
(19, 10, 5),
(20, 10, 4);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Выдача`
--
ALTER TABLE `Выдача`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_экземпляра` (`ID_экземпляра`),
  ADD KEY `ID_читателя` (`ID_читателя`);

--
-- Индексы таблицы `Издание`
--
ALTER TABLE `Издание`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `Читатель`
--
ALTER TABLE `Читатель`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `Экземпляр`
--
ALTER TABLE `Экземпляр`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_издания` (`ID_издания`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Выдача`
--
ALTER TABLE `Выдача`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'ID выдачи', AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT для таблицы `Издание`
--
ALTER TABLE `Издание`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'ID издания', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `Читатель`
--
ALTER TABLE `Читатель`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'ID читателя', AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `Экземпляр`
--
ALTER TABLE `Экземпляр`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'ID экземпляра', AUTO_INCREMENT=21;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `Выдача`
--
ALTER TABLE `Выдача`
  ADD CONSTRAINT `Выдача_ibfk_1` FOREIGN KEY (`ID_экземпляра`) REFERENCES `Экземпляр` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Выдача_ibfk_2` FOREIGN KEY (`ID_читателя`) REFERENCES `Читатель` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Экземпляр`
--
ALTER TABLE `Экземпляр`
  ADD CONSTRAINT `Экземпляр_ibfk_1` FOREIGN KEY (`ID_издания`) REFERENCES `Издание` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
