<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php if (!empty($delivery) && is_array($delivery)) : ?>
            <h2>Все выдачи:</h2>
            <div class="d-flex justify-content-between mb-2">
                <?= $pager->links('group1','my_page') ?>
                <?= form_open('DeliveryController/viewAllWithBook', ['style' => 'display: flex']); ?>
                <select name="per_page" class="ml-3" aria-label="per_page">
                    <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                    <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                    <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                    <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
                </select>
                <button class="btn btn-outline-success" type="submit" class="btn btn-primary">На странице</button>
                </form>
                <?= form_open('DeliveryController/viewAllWithBook',['style' => 'display: flex']); ?>
                <input type="text" class="form-control ml-3" name="search" placeholder="ФИО или название книги" aria-label="search" value="<?= $search; ?>">
                <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
                </form>
            </div>

            <table class="table table-striped">
                <thead>

                <th scope="col">ФИО</th>
                <th scope="col">Книга</th>
                <th scope="col">Управление</th>

                </thead>
                <tbody>
                <?php foreach ($delivery as $item): ?>
                    <tr>
                        <td><?= esc($item['ФИО']); ?></td>
                        <td><?= esc($item['Наименование']); ?></td>
                        <td>
                            <a href="<?= base_url()?>/delivery/view/<?= esc($item['ID']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                            <a href="<?= base_url()?>/delivery/edit/<?= esc($item['ID']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                            <a href="<?= base_url()?>/delivery/delete/<?= esc($item['ID']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        <?php else : ?>
            <div class="text-center">
                <p>Рейтинги не найдены </p>
                <a class="btn btn-primary btn-lg" href="<?= base_url()?>/delivery/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Добавить выдачу</a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>