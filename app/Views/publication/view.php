<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($arr_publication)) : ?>
        <div class="card mb-3" style="max-width: 540px;">
            <div class="row">
                <div class="col-md-4 d-flex align-items-center">
                    <?php if ($arr_publication['id'] == 1) : ?>
                        <img height="150" width="100" src="https://sun9-5.userapi.com/impg/5X9DTkfvq3dnMjo_4s1q9V3LyHxZJ_YnROJbGg/U30wDymFEuE.jpg?size=284x430&quality=96&sign=4b146fedbfc873071ef4579e0370392f&type=album" alt="<?= esc($arr_publication['Наименование']); ?>">
                    <?php elseif ($arr_publication['id'] == 2) : ?>
                        <img height="150" width="100" src="https://sun9-70.userapi.com/impg/05pg1xCCnBFLZccxZkhiohnWV5oGXFdp8W9JlA/-uGIf6ARTwI.jpg?size=148x241&quality=96&sign=4ae00f39b184257e9f1f613b6455ee3f&type=album" alt="<?= esc($arr_publication['Наименование']); ?>">
                    <?php elseif ($arr_publication['id'] == 3) : ?>
                        <img height="150" width="100" src="https://sun9-60.userapi.com/impg/VvNoie8Ve9wYy-hV4rkRaGSRWFGIRYpcZkeDfA/j1jM9wVVIlE.jpg?size=256x401&quality=96&sign=0be2b526d10ec90f661aa7d33c7ef2ca&type=album" alt="<?= esc($arr_publication['Наименование']); ?>">
                    <?php elseif ($arr_publication['id'] == 4) : ?>
                        <img height="150" width="100" src="https://sun9-42.userapi.com/impg/K5bb14d_b2zZiYnumG0A2_VEb83wIXHK2D8ebQ/HjGzfOP9bFE.jpg?size=300x420&quality=96&sign=7d9f18d1b9925c0c7c8db5ee7e8113c5&type=album" alt="<?= esc($arr_publication['Наименование']); ?>">
                    <?php elseif ($arr_publication['id'] == 5) : ?>
                        <img height="150" width="100" src="https://sun9-17.userapi.com/impg/NedDb_eX1nZzcLn-OdlDORsSKBRyFRJKAvdWlg/VArFdmOY7Yg.jpg?size=516x734&quality=96&sign=6d9aa48d8c3de91f78c4d984b3e0bf3f&type=album" alt="<?= esc($arr_publication['Наименование']); ?>">
                    <?php elseif ($arr_publication['id'] == 6) : ?>
                        <img height="150" width="100" src="https://sun9-48.userapi.com/impg/MnEdJcLzR7FhQwlKQFVv03OoBvfAX7KzKNaLtg/ethUu-LiCoU.jpg?size=522x742&quality=96&sign=c3d4b3a44bb16e8069dfc7f8ec47eaeb&type=album" alt="<?= esc($arr_publication['Наименование']); ?>">
                    <?php elseif ($arr_publication['id'] == 7) : ?>
                        <img height="150" width="100" src="https://sun9-50.userapi.com/impg/Qv0IuzC75attawrwtYLFYuJpyrLK06ZY48GZjA/MDI5tt1Zkxk.jpg?size=528x747&quality=96&sign=7bbce4dc9dff267baa4d0ce6f1a34a91&type=album" alt="<?= esc($arr_publication['Наименование']); ?>">
                    <?php elseif ($arr_publication['id'] == 8) : ?>
                        <img height="150" width="100" src="https://sun3-13.userapi.com/impg/fLWBhHB11IqqIXjAcM9BvDb-wbzkvV_7jywAlg/VMCdvQ_QKqw.jpg?size=528x744&quality=96&sign=9aaa5450674a531673d7292de11e4298&type=album" alt="<?= esc($arr_publication['Наименование']); ?>">
                    <?php elseif ($arr_publication['id'] == 9) : ?>
                        <img height="150" width="100" src="https://sun9-26.userapi.com/impg/TzPAcmmkZrdlthsuMvjrgjxzsQEA22D4DKw29g/KGriwiVtHIg.jpg?size=533x746&quality=96&sign=db9dd7ef41785714040f13245ec3cba5&type=album" alt="<?= esc($arr_publication['Наименование']); ?>">
                    <?php elseif ($arr_publication['id'] == 10) : ?>
                        <img height="150" width="100" src="https://sun9-28.userapi.com/impg/PEyAXU3zXlhGdhcjqlhnhEZGxNFf7UOGUnis1g/uWatGdxIx8w.jpg?size=653x745&quality=96&sign=40ba204e3d60a9ea32eee99cf287fa24&type=album" alt="<?= esc($arr_publication['Наименование']); ?>">
                    <?php else:?>
                        <img height="150" src="<?= esc($arr_publication['picture_url']); ?>" alt="<?= esc($arr_publication['id']); ?>">
                    <?php endif ?>
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title"><?= esc($arr_publication['Наименование']); ?></h5>
                        <p class="card-text"><?= esc($arr_publication['Автор']); ?></p>
                    </div>
                    <div>
                    <a href="<?= base_url()?>/publication/edit/<?= esc($arr_publication['id']); ?>" class="btn btn-primary" style="margin-bottom:15px;">Изменить</a>
                    <a href="<?= base_url()?>/publication/delete/<?= esc($arr_publication['id']); ?>" class="btn btn-danger" style="margin-bottom:15px;">Удалить</a>
                    </div>
                </div>
            </div>
        </div>
    <?php else : ?>
        <p>Книга не найдена.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
