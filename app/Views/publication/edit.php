<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('publication/update'); ?>
        <input type="hidden" name="id" value="<?= $arr_publication["id"] ?>">

        <div class="form-group">
            <label for="name">Название книги</label>
            <input type="text" class="form-control <?= ($validation->hasError('Наименование')) ? 'is-invalid' : ''; ?>" name="Наименование"
                   value="<?= $arr_publication["Наименование"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Наименование') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="name">Автор</label>
            <input type="text" class="form-control <?= ($validation->hasError('Автор')) ? 'is-invalid' : ''; ?>" name="Автор"
                   value="<?= $arr_publication["Автор"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Автор') ?>
            </div>
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>