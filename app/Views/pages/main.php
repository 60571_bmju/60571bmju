<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="jumbotron text-center">
        <img class="mb-4" src="https://www.flaticon.com/svg/vstatic/svg/4163/4163723.svg?token=exp=1615117735~hmac=1a1c6e3ab8adf2a9d7e087cb74560738" alt="" width="90" height="90"><h1 class="display-4">BooksForCoffee</h1>
        <p class="lead">Это приложение поможет определиться с выбором книги любого жанра, который будет интересен даже юному читателю.</p>
        <a class="btn btn-primary btn-lg" href="auth/login" role="button">Войти</a>
    </div>
<?= $this->endSection() ?>

