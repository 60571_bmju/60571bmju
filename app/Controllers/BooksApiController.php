<?php

namespace App\Controllers;
use App\Models\PublicationModel;
use App\Models\OAuthModel;

use CodeIgniter\RESTful\ResourceController;
use App\Services\OAuth;
use OAuth2\Request;

class BooksApiController extends ResourceController
{

    public function __construct()
    {
        Header('Access-Control-Allow-Origin: *');
        Header('Access-Control-Allow-Headers: *');
        Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
    }

//    protected $model = 'App\Models\PublicationModel';
//    protected $format = 'json';
    protected $oauth;

    public function showBooks() //Отображение всех записей
    {
        $oauth = new OAuth();
        if ($oauth->isLoggedIn()) {
            return $this->respond($this->model->getPublication());
        }
        $oauth->server->getResponse()->send();
    }
    public function book() //Отображение всех записей
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = new PublicationModel();
            $per_page = $this->request->getPost('per_page');
            $search = $this->request->getPost('search');
            if($search == null) {
                $search = '';
            }
            $data = $model->getBooks('', $search,  $per_page);
            return $this->respond(['books' => $data, 'pager' => $model->pager->getDetails('group1')]);
        } else $this->oauth->server->getResponse()->send();
    }
    public function store()
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = new PublicationModel();
            if ($this->request->getMethod() === 'post' && $this->validate([
                    'Наименование' => 'required|min_length[3]|max_length[255]',
                    'Автор' => 'required|min_length[3]|max_length[255]',
                ])) {
                //подготовка данных для модели
                $data = [
                    'Наименование' => $this->request->getPost('Наименование'),
                    'Автор' => $this->request->getPost('Автор'),
                ];
                $model->save($data);
                return $this->respondCreated(null, 'Book created successfully');
            } else {
                return $this->respond($this->validator->getErrors());
            }
        } else $this->oauth->server->getResponse()->send();
    }
    public function delete($id = null)
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isloggedIn()) {

            $model = new PublicationModel();
            $model->delete($id);
            return $this->respondDeleted(null, 'Book deleted successfully');
        }

        $this->oauth->server->getResponse()->send();
    }


}