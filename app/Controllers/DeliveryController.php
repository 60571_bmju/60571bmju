<?php namespace App\Controllers;

use App\Models\DeliveryModel;


class DeliveryController extends BaseController
{
    public function viewAllWithBook()
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;

            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;

            helper(['form','url']);
            $model = new DeliveryModel();
            $data['delivery'] = $model->getDeliveryWithBook(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('Delivery/delivery', $this->withIon($data));
//            var_dump($data['delivery']);
        }
        else
        {
            session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }

//
//    public function create()
//    {
//        if (!$this->ionAuth->loggedIn())
//        {
//            return redirect()->to('/auth/login');
//        }
//        helper(['form']);
//        $data ['validation'] = \Config\Services::validation();
//        echo view('Delivery/create', $this->withIon($data));
//    }
//
//
//    public function store()
//    {
//        helper(['form','url']);
//
//        if ($this->request->getMethod() === 'post' && $this->validate([
//                'ФИО' => 'required|min_length[3]|max_length[255]',
//                'Наименование'  => 'required|min_length[3]|max_length[255]',
//            ]))
//        {
//            $model = new DeliveryModel();
//            $model->save([
//                'ФИО' => $this->request->getPost('ФИО'),
//                'Наименование' => $this->request->getPost('Наименование'),
//            ]);
//            session()->setFlashdata('message', lang('Curating.rating_create_success'));
//            return redirect()->to('/Delivery');
//        }
//        else
//        {
//            return redirect()->to('/Delivery/create')->withInput();
//        }
//    }
//
//
//    public function edit($id)
//    {
//        if (!$this->ionAuth->loggedIn())
//        {
//            return redirect()->to('/auth/login');
//        }
//        $model = new DeliveryModel();
//
//        helper(['form']);
//        $data ['delivery'] = $model->getPublication($id);
//        $data ['validation'] = \Config\Services::validation();
//        echo view('rating/edit', $this->withIon($data));
//
//    }

}