<?php namespace App\Controllers;

use App\Models\PublicationModel;
use Aws\S3\S3Client;

class Publication extends BaseController
{
    public function index() //Отображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PublicationModel();
        $data ['arr_publication'] = $model->getPublication();
        echo view('publication/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PublicationModel();
        $data ['arr_publication'] = $model->getPublication($id);
        echo view('publication/view', $this->withIon($data));
    }


    public function create()
    {
        if (!$this->ionAuth->isAdmin())
        {
            session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('publication/create', $this->withIon($data));
    }


    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'Автор' => 'required|min_length[3]|max_length[255]',
                'Наименование'  => 'required|min_length[3]|max_length[255]',
                'picture_url'  => 'is_image[picture_url]|max_size[picture_url,1024]',
            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture_url');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }
            $model = new PublicationModel();

            $data = [
                'Автор' => $this->request->getPost('Автор'),
                'Наименование' => $this->request->getPost('Наименование'),
            ];
            //если изображение было загружено и была получена ссылка на него то даобавить ссылку в данные модели
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model -> save($data);
//                'Автор' => $this->request->getPost('Автор'),
//                'Наименование' => $this->request->getPost('Наименование'),
//            ]);
            session()->setFlashdata('message', lang('Издание успешно добавлено'));
            return redirect()->to('/publication');
        }
        else
        {
            return redirect()->to('/publication/create')->withInput();
        }
    }


    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PublicationModel();

        helper(['form']);
        $data ['arr_publication'] = $model->getPublication($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('publication/edit', $this->withIon($data));

    }


    public function update()
    {
        helper(['form','url']);
        echo '/publication/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'Автор'  => 'required|min_length[3]|max_length[255]',
                'Наименование' => 'required|min_length[3]|max_length[255]',
            ]))
        {
            $model = new PublicationModel();
            $data = [

                'Автор' => $this->request->getPost('Автор'),
                'Наименование' => $this->request->getPost('Наименование'),
            ];
            $model->update($this->request->getPost('id'), $data);
            session()->setFlashdata('message', lang('Редактирование издания успешно выполнено'));

            return redirect()->to('/publication');
        }
        else
        {
            return redirect()->to('/publication/edit/'.$this->request->getPost('id'))->withInput();
        }
    }


    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PublicationModel();
        $model->delete($id);
        session()->setFlashdata('message', lang('Удаление издания успешно выполнено'));
        return redirect()->to('/publication');

    }

}