<?php namespace App\Models;
use CodeIgniter\Model;



class DeliveryModel extends Model
{
    protected $table = 'Выдача';

    public function getDeliveryWithBook($id = null, $search = '')
    {
        $builder = $this->select('*')->join('Экземпляр','Выдача.ID_экземпляра = Экземпляр.ID')->join('Издание','Экземпляр.ID_издания = Издание.ID')->join('Читатель','Выдача.ID_читателя = Читатель.ID')->like('ФИО', $search,'both', null, true)->orlike('Наименование',$search,'both',null,true);;
        if (!is_null($id))
        {
            return $builder->where(['Выдача.ID' => $id])->first();
        }
        return $builder;
    }

}