<?php

namespace App\Models;
use CodeIgniter\Model;

class PublicationModel extends Model
{
    protected $table = 'Издание'; //таблица, связанная с моделью
    protected $allowedFields = ['Автор', 'Наименование', 'picture_url'];

    public function getPublication($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getBooks($user_id = null, $search = '', $per_page = null)
    {
        $model = $this->like('Наименование', $search, 'both');
//        if (!is_null($user_id)) {
//            $model = $model->where('user_id', $user_id);
//        }
        // Пагинация
        return $model->paginate($per_page, 'group1');
    }



}
