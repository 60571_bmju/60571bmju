<?php namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('498103266019-eqbccfjjp3doa3smr04trp4t8bhvr4qi.apps.googleusercontent.com');
        $this->google_client->setClientSecret('_vTq0f47pdvE267OsUGi-n0F');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }

}