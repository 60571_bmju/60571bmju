<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MyMigration extends Migration
{


    public function up()
    {




        // Издание
        if (!$this->db->tableexists('Издание'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'Наименование' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
                'Автор' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),

            ));
            // create table
            $this->forge->createtable('Издание', TRUE);
        }


        if (!$this->db->tableexists('Читатель'))
        {
            //Читатель
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ФИО' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),

            ));
            // create table
            $this->forge->createtable('Читатель', TRUE);
        }


        // Экземпляр
        if (!$this->db->tableexists('Экземпляр'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ID_издания' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'Коэффициент_износа' => array('type' => 'INT', 'null' => FALSE)

            ));
            $this->forge->addForeignKey('ID_издания','Издание','ID','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('Экземпляр', TRUE);
        }

        if (!$this->db->tableexists('Выдача'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ID_экземпляра' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'ID_читателя' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'Дата_выдачи' => array('type' => 'DATE', 'null' => FALSE),
                'Дата_возврата_план' => array('type' => 'DATE', 'null' => FALSE),
                'Дата_возврата_факт' => array('type' => 'DATE', 'null' => TRUE)

            ));
            $this->forge->addForeignKey('ID_экземпляра','Экземпляр','ID','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('ID_читателя','Читатель','ID','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('Выдача', TRUE);
        }
    }

    //--------------------------------------------------------------------

    public function down()
    {

        $this->forge->droptable('Выдача');
        $this->forge->droptable('Издание');
        $this->forge->droptable('Читатель');
        $this->forge->droptable('Экземпляр');
    }
}
